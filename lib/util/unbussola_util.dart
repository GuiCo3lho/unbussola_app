import 'package:flutter/material.dart';
import 'package:unbussola_app/widgets/expansions.dart';
import 'package:unbussola_app/util/cores.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Util {
  static Drawer createMenuDrawer() {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
              margin: EdgeInsets.zero,
              height: 160.0,
              child: DrawerHeader(
                  padding: EdgeInsets.zero,
                  margin: EdgeInsets.zero,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(top: 40.0, left: 16.0),
                          height: 64.0,
                          width: 64.0,
                          ),
                    ],
                  ),
                  decoration: BoxDecoration(color: Colors.blue))),
          createMenuDrawerOption("GuiCo3lho", Colors.lightGreen,[
            createMenuDrawerItem("Meu perfil"),
            createMenuDrawerItem("Meus Eventos"),
            createMenuDrawerItem("Lugares Favoritos"),
            createMenuDrawerItem("Chat")
          ]),
          createMenuDrawerOption(
              "Atalhos",
              Colors.blue,
              [
                createMenuDrawerItem("Cadastrar um Evento"),
                createMenuDrawerItem("Procurar um Evento"),
                createMenuDrawerItem("Favoritar um Evento")
              ],
              leadingIcon: Icon(Icons.info)),
          createMenuDrawerOption(
              "Informações",
              Colors.blue,
              [
                createMenuDrawerItem("Dicas"),
                createMenuDrawerItem("Eventos"),
                createMenuDrawerItem("Legislação"),
                createMenuDrawerItem("Termos de Serviço")
              ],
              leadingIcon: Icon(Icons.info)),
          createMenuDrawerOption("Configurações", Cores.cinzaFracoMenu,
              [createMenuDrawerItem("Privacidade")],
              leadingIcon: Icon(Icons.settings))
        ],
      ),
    );
  }


  static MaterialApp createDefaultMaterialApp(String appBarTitle, Widget body) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "UnBussola App",
      home: Scaffold(
          appBar: createAppBar(appBarTitle),
          drawer: createMenuDrawer(),
          body: body),
    );
  }

  static AppBar createAppBar(String texto) {
    return AppBar(
      automaticallyImplyLeading: true,
      title: Text(
        texto,
        style: TextStyle(fontSize: 20.0),
      ),
    );
  }

  static Container createMenuDrawerOption(String title, Color headerBackgroundColor,
      List<Widget> body,
      {Widget leadingIcon, bool initiallyExpanded: true}) {
    List<Widget> widgetSeparado = List();

    for (int i = 0; i < body.length; ++i) {
      widgetSeparado.add(Divider());
      widgetSeparado.add(body[i]);
    }


    return Container(
        margin: EdgeInsets.zero,
        child: CustomExpansionTile(
          initiallyExpanded: initiallyExpanded,
          leading: leadingIcon,
          headerBackgroundColor: headerBackgroundColor,
          title: Text(
            title,
            style: TextStyle(color: Cores.pretoForte),
          ),
          children: widgetSeparado,
        ));
  }

  static RaisedButton createRaisedButton(String text, Function onPressed,
      {double padding: 40.0}) {
    return RaisedButton(
      elevation: 2,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: padding),
        child: Text(
          text,
          style: TextStyle(
              fontFamily: "Roboto", fontSize: 12.0, color: Cores.pretoForte),
        ),
      ),
      onPressed: onPressed,
    );
  }

  static Widget createNCheckBoxColumn(
  List checkBoxLabel, List<Function(bool)> callbacks) {
  int posCallbacks = 0;
  List<Widget> listOfRows = List();

  for (int i = 0; i < checkBoxLabel.length; i++) {
  listOfRows.add(Row(
  children: <Widget>[
  Checkbox(
  value: false,
  onChanged: callbacks[posCallbacks++],
  ),
  Text(checkBoxLabel[i])
  ],
  ));
  }

  return Container(
  margin: EdgeInsets.only(left: 10.0),
  child: Column(children: listOfRows));
  }

  static Widget createFieldLabel(String texto,
      {double fontSize: 12.0,
        FontWeight fontWeight: FontWeight.w400,
        Color textColor: Cores.amareloEscuro,
        double top: 8.0,
        double bottom: 8.0}) {
    return Container(
      child: Text(texto,
          style: TextStyle(
            fontSize: fontSize,
            color: textColor,
            fontFamily: "Roboto",
            fontWeight: fontWeight,
          )),
      margin: EdgeInsets.only(left: 24.0, top: top, bottom: bottom),
    );
  }

  static Widget createMenuDrawerItem(String text) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 30.0, top: 10, bottom: 10),
          child: Text(
            text,
            textAlign: TextAlign.left,
            textDirection: TextDirection.ltr,
            style: TextStyle(
                fontSize: 14,
                fontFamily: "Roboto",
                fontWeight: FontWeight.w400,
                color: Cores.pretoForte),
          ),
        ),
      ],
    );
  }
}

